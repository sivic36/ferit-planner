var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

//Any requests to this controller must pass through this 'use' function
//Copy and pasted from method-override
router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

const User = mongoose.model('User');
const bcrypt = require('bcryptjs');
const passport = require('passport');
let constants = require('../config/constants');

var studiji;

//Users - login
router.route('/login')
  .get(function (req, res, next) {
    if(req.user==null)
      res.render('users/login');
    else
      res.redirect('/schedule');
  })
  .post(function(req, res, next){
    passport.authenticate('local', {
      successRedirect: '/schedule',
      failureRedirect: '/users/login',
      failureFlash: true
    })(req, res, next);
  });

//Users - register
router.route('/register')
  .get(function (req, res, next) {
    if(req.user==null)
      setRegistrationLayout(res);
    else
      res.redirect('/schedule');
  })
  .post(function(req, res){

    var name = req.body.name;
    var email = req.body.email;
    var pass = req.body.password;
    var pass2 = req.body.confirmPassword;
    var studijIndex = req.body.studij;
    var smjerIndex = req.body.smjer;

    let errors = [];

    //VALIDATION
    if(pass!==pass2)
      errors.push({msg: 'Lozinke se ne podudaraju'});
    if(pass.length < 6)
      errors.push({msg: 'Lozinka treba sadržavati najmanje 6 znakova!'});
    if(!studijIndex || !smjerIndex)
      errors.push({msg:'Odaberi studij i smjer!'});
    else{
      var smjer = studiji[studijIndex].smjerovi[smjerIndex];
      var studij = {
        naziv: studiji[studijIndex].naziv,
        smjer: smjer
      }
      console.log("Studij: "+studij.naziv+"\nSmjer: "+studij.smjer.naziv+"\nLink: "+studij.smjer.link);
    }

    //Check errors
    if(errors.length>0){
      res.render('users/register', {errors, name, email, studiji});
    }
    else{ //Register user
      //Check if exists
      User.findOne({email: email})
        .then(user => {
          if(user){
            errors.push({msg: 'Korisnik s navedenom e-mail adresom već postoji!'});
            res.render('users/register', {errors, name, email, studiji});
          }
          else{ //Save user to db
            var newUser = new User({
              name: name,
              email: email,
              password: pass,
              studij: studij
            });
            
            //hash the password
            bcrypt.genSalt(10, (err, salt)=> 
              bcrypt.hash(newUser.password, salt, (err, hash) => {
                if(err) throw err;
                newUser.password = hash;
                newUser.save()
                  .then(user => {
                    req.flash('success_msg', 'Registriran si i možeš se prijaviti!')
                    res.redirect('login');
                  })
                  .catch(err => console.log(err));
              }))
          }
        })
    }
  });

function setRegistrationLayout(res){
  if(studiji){
    res.render('users/register', {studiji: studiji});
  }
  else{
    var HTMLParser = require('node-html-parser');
    var request = require('request');
    request.get(constants.baseUrl, function(err,response,body){
      var root = HTMLParser.parse(body);
      var data = root.querySelector('#odabir-studija-godine-div').firstChild.childNodes;
      studiji = [];
      for(var i=1; i<data.length; i++){
        var smjerovi=[];
        for(var j=2; j<data[i].childNodes.length; j++){
          var href = data[i].childNodes[j].attributes.href;
          var s = href.split('/');
          var smjer = {
            naziv: data[i].childNodes[j].rawText,
            link: s[s.length-1]
          };
          smjerovi.push(smjer);
        }
        var studij = {
          naziv: data[i].firstChild.rawText,
          smjerovi: smjerovi
        };
        studiji.push(studij);
      }
      res.render('users/register', {studiji: studiji});
    });
  }
}

router.route('/logout')
  .get(function(req, res, next){
    req.logout();
    req.flash('success_msg', 'Odjavljeni ste');
    res.redirect('/users/login');
  });

module.exports = router;
