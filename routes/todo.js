var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST
const { ensureAuthenticated } = require('../config/auth');
const Task = mongoose.model('Task');

//Any requests to this controller must pass through this 'use' function
//Copy and pasted from method-override
router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}))

/* GET To-Do page. */
router.route('/')
  .get(ensureAuthenticated, function(req, res, next) {
    //Get tasks for user
    Task.find({ user: req.user._id }, function(err, tasks){
      if(err) return console.log(err);
      else{
        var todos=[];
        var completed=[];
        tasks.forEach(function(task){
          if(task.completed) completed.push(task);
          else todos.push(task);
        });
        res.format({
          html: function () {res.render("todo/index", {todos, completed});},
          json: function () {res.json({todos, completed});}
        });
      } 
    });
  });

router.route('/new')
  .post(ensureAuthenticated, function(req, res, next){
    //Adding new task (to-do)
    var newTask = {
      text: req.body.task,
      completed: false,
      user: req.user._id
    };
    Task.create(newTask, function (err, task){
      if (err) return console.log(err);
      else sendResponse(res);
    });
  });

router.route('/:id')
  .put(ensureAuthenticated, function(req, res){
    Task.findByIdAndUpdate(req.id, req.body, function (err, task){
      if (err) return console.log(err);
      else sendResponse(res);
    })
  })
  .delete(ensureAuthenticated, function(req, res){
    Task.findByIdAndRemove(req.id, function (err, task){
      if (err) return console.log(err);
      else sendResponse(res);
    })
  })
  .get(ensureAuthenticated, function(req, res){
    Task.findById(req.id, function(err, task){
      if(err) return console.log(err)
      else res.render('todo/form', {task});
    })
  });

// route middleware to validate :id
router.param('id', function (req, res, next, id) {
  //Validation - find ID in DB
  Task.findById(id, function (err, task) {
      if (err) setErrorStatus(res, id);
      else {
          req.id = id;
          next();
      }
  });
});

function sendResponse(res){
  res.format({
    html: function () {
        res.redirect("/todo");
    },
    json: function () {
        res.json(task);
    }
  });
}

function setErrorStatus(res, id){
  console.log(id + ' was not found');
  res.status(404)
  var err = new Error('Not Found');
  err.status = 404;
  res.format({
      html: function () {
          next(err);
      },
      json: function () {
          res.json({ message: err.status + ' ' + err });
      }
  });
}

module.exports = router;