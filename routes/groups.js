var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST
const { ensureAuthenticated } = require('../config/auth');
const Event = mongoose.model('Event');
const User = mongoose.model('User');

//Any requests to this controller must pass through this 'use' function
//Copy and pasted from method-override
router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}))

/* GET Groups page. */
router.route('/')
  .get(ensureAuthenticated, function(req, res, next) {
    var groupevents = [];
    var pendinggroupevents = [];
    var mygroupevents = [];
    //Get group events for user
    Event.find({$where:'this.members.length>0'}, function(err, events){
      //console.log(events)
      if(err) return console.log(err);
      else if(events){
        events.forEach(function(event){
          //Get user group events
          if(event.user == req.user._id)
            mygroupevents.push(event)
          
          //Get group events user is member (not creator)
          else
            event.members.forEach(function(member){
              if(member.email == req.user.email){
                if(member.accepted)
                  groupevents.push(event);
                else 
                  pendinggroupevents.push(event);
              }
            })
        })
        res.format({
          html: function(){res.render("groups/index", {groupevents, pendinggroupevents, mygroupevents});},
          json: function (){res.json({groupevents, pendinggroupevents, mygroupevents});}
        })
      }
    });
  });

//Creating new group event
router.route('/new')
  .get(ensureAuthenticated, function(req, res){
    res.render('groups/new');
  })
  .post(ensureAuthenticated, function(req, res){
    //Add new group event
    var members;
    var member_emails = JSON.parse(req.body.members);
    if(member_emails.length){
      members=[];
      member_emails.forEach(function(email){
        members.push({ email: email });
      })
      var newGroupEvent = {
        text : req.body.text,
        start_date : req.body.start_date, 
        end_date : req.body.end_date,
        color : req.body.color,
        user : req.user._id, 
        members : members
      };
      Event.create(newGroupEvent, function(err, event){
        if(err) return console.log(err);
        else sendResponse(res);
      });
    }
    else{
      var error_msg = 'Please add some members';
      res.render('groups/new', {old: req.body, error_msg})
    }
  });

//Handling member e-mail validation (only registered users can be event members)
router.route('/check')
  .get(ensureAuthenticated, function(req, res){
    User.findOne({email: req.query.email}, function(err, user){
      if(err)return console.log(err);
      else {
        if(user)
          if(user.email!=req.user.email) res.json(true);
          else res.json(false)
        else
          res.json(false)
      }
    })
  });

//Editing group event
router.route('/:id')
  .get(ensureAuthenticated, function(req, res, next){
    Event.findById(req.id, function(err, event){
      if(err) return console.log(err);
      else {
          res.render('groups/edit', {event})
      }
    })
  })
  .delete(ensureAuthenticated, function(req, res, next){
    Event.findByIdAndRemove(req.id, function(err, event){
      if (err) return console.log(err);
      else sendResponse(res);
    })
  })
  .put(ensureAuthenticated, function(req, res, next){
    var members = JSON.parse(req.body.members);
    if(members.length){
      //Update event with req.body data
      Event.findById(req.id, function (err, event){
        if (err) return console.log(err);
        else {
          event.update({
            text: req.body.text,
            start_date: req.body.start_date,
            end_date: req.body.end_date,
            color: req.body.color,
            user: req.user._id,
            members: members
          }, function(err){
            if(err) return console.log(err);
            else sendResponse(res);
          });
        }
      });
    }
    else{
      req.flash('error_msg', 'Please add some members');
      res.redirect('/groups/'+req.id)
    }
  });

//Show event
router.route('/info/:id')
.get(ensureAuthenticated, function(req, res, next){
  Event.findById(req.id, function(err, event){
    if(err) return console.log(err);
    else {
      User.findById(event.user, function(err, user){
          event.user = user.email;
          res.render('groups/form', {event: event})
      })
    }
  })
})

//Handling pending events
router.route('/:id/members')
  .put(ensureAuthenticated, function(req, res){
    Event.findById(req.id, function(err, event){
      if(err) return console.log(err);
      else{
        var members = event.members;
        //Find member index for logged user
        var index = members.findIndex(function(member){
           return member.email == req.user.email 
        });
        //Accept invitation
        if(req.body.accepted=='true'){
          members[index].accepted=true;
          //update user.groups property
          var groups=[];
          if(req.user.groups)
            groups=req.user.groups;
          groups.push(event._id)
          req.user.update({
            groups: groups
          }, function(err){
            if(err) return console.log(err);
          })
        }
        //Deny
        else if(req.body.accepted=='false'){
          //remove user as member
          members.splice(index, 1);
          //update user.groups property
          var groups=req.user.groups;
          var i = groups.indexOf(event._id);
          groups.splice(i, 1);
          req.user.update({
            groups: groups
          }, function(err){
            if(err) return console.log(err);
          })
        }

        event.update({ members: members }, function(err){
          if(err) return console.error(err);
          else{
              res.redirect('/groups');
          }
        })
      }
    })
  })


router.param('id', function(req, res, next, id){
  //Validation
  Event.findById(id, function(err, event){
    if(err) setErrorStatus(res, id);
    else{
      req.id = id;
      next();
    }
  })
});

function sendResponse(res){
  res.format({
    html: function () {
        res.redirect("/groups");
    },
    json: function () {
        res.json(event);
    }
  });
}

function setErrorStatus(res, id){
  console.log(id + ' was not found');
  res.status(404)
  var err = new Error('Not Found');
  err.status = 404;
  res.format({
      html: function () {
          next(err);
      },
      json: function () {
          res.json({ message: err.status + ' ' + err });
      }
  });
}

module.exports = router;