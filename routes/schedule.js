var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var HTMLParser = require('node-html-parser');
var request = require('request');
const { ensureAuthenticated } = require('../config/auth');
let constants = require('../config/constants');
const Event = mongoose.model('Event');
var categories = [
    { key: '#2979FF', label:'Appointment'},
    { key: '#4CAF50', label:'Fun'},
    { key: '#FFCA28', label:'Work'},
    { key: '#F44336', label:'Study'},
    { key: '#009688', label:'Other'}
];

router.route('/')
    .get(ensureAuthenticated, function(req, res){
        res.render('schedule/index', {categories});
    });

router.route('/data')
    .get(ensureAuthenticated, function(req, res){
        var events=[];
        getUserEvents(req, res, events);
    })
    .post(ensureAuthenticated, function(req, res){
        var data = req.body;

        var mode = data['!nativeeditor_status'];
        var sid = data.id;
        var tid = sid;

        //modify event data object for MongoDB
        delete data.id;
        delete data.gr_id;
        delete data["!nativeeditor_status"];
        data.user = req.user._id;

        if (mode == "updated")
            Event.findByIdAndUpdate(sid, data, callback);
        else if (mode == "inserted"){
            Event.create(data, callback);
        }
        else if (mode == "deleted")
            Event.findByIdAndRemove(sid, callback);
        else
            res.send("Not supported operation");
        
        function callback(err, result){
            if (err){
                mode = "error";
                console.log(err);
            }
            else if (mode == "inserted"){
                tid = result._id;
            }
            res.setHeader("Content-Type","application/json");
            res.send({action: mode, sid: sid, tid: tid});
        }
    });

function getUserEvents(req, res, events){
    //Get events created by user
    Event.find({
        user: req.user._id,
        //start_date: {$gte:req.query.from},
        //end_date: {$lt:req.query.to}
    }, function(err, data){
        if(err)
            return console.log("Error getting events: "+err);
        else {
            //modify event data for scheduler (id)
            data.forEach(function(item){
                var event = {
                    _id: item._id,
                    id: item._id,
                    text: item.text,
                    start_date: item.start_date,
                    end_date: item.end_date,
                    color: item.color,
                    user: item.user,
                    uneditable: false
                }
                events.push(event);
            });
        }
        //Get events user is member of
        Event.find({
            _id: {$in: req.user.groups}
        }, function(err, data){
            if(err)
                return console.log("Error getting events: "+err);
            else {
                //modify event data for scheduler (id)
                data.forEach(function(item){
                    var event = {
                        _id: item._id,
                        id: item._id,
                        text: item.text,
                        start_date: item.start_date,
                        end_date: item.end_date,
                        color: item.color,
                        user: item.user,
                        uneditable: true
                    }
                    events.push(event);
                });
            }
            //Get FERIT Raspored events
            getFeritRasporedData(req, res, events);
        });
    });
}

function getFeritRasporedData(req, res, events){
    var link = constants.baseUrl + '/' + req.query.from + '/' + req.user.studij.smjer.link;
    console.log(link);
    request.get(link, function(err,response,body){
        var root = HTMLParser.parse(body);
        var data = root.querySelector('#raspored').querySelectorAll('.dan');
        //console.log("\n---RASPORED---\n");
        for(var i=1;i<data.length;i++){

            var danInfo = data[i].childNodes[0].childNodes[1].attributes;
            //console.log("\n"+danInfo.href+" ("+danInfo.name+"):");

            var blokovi = data[i].childNodes[1].childNodes;
            if(blokovi.length){
                for(var j=0;j<blokovi.length;j++){
                    var s = blokovi[j].childNodes[1].childNodes[3].structuredText.split('\n');
                    var time = s[2].split(' - ');
                    var event = {
                        text: s[0]+'\n'+s[1],
                        start_date: danInfo.name+'T'+time[0],
                        end_date: danInfo.name+'T'+time[1],
                        color: '#aaaaaaaa',
                        uneditable: true
                    };
                    //console.log(event);
                    events.push(event);
                }
            }
        }
        //output response to scheduler
        res.send(events);
    });
}

module.exports = router;