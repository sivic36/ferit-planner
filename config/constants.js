'use strict';

require('dotenv').config();
console.log("FERIT base URL: " + String(process.env.FERIT_BASE_URL))

let constants = {
    baseUrl: String(process.env.FERIT_BASE_URL)
};

module.exports = Object.freeze(constants);