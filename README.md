# FERIT Planner

<img src="public/images/favicon.ico">

Student project developed within Advanced Web Programming course @[FERIT](https://www.ferit.unios.hr/)

## Prerequisites

* MongoDB server installed

You can install MongoDB server locally or in the cloud, just make sure to put your `MONGODB_URL` in environment variables (`.env` file).

* FERIT Base URL

Make sure to set `FERIT_BASE_URL` env variable which points to the `FERIT` schedule page, so your `.env` file should look something like this:
```
MONGODB_URL=mongodb://localhost:27017/db
FERIT_BASE_URL=https://www.ferit.unios.hr/2021/studenti/raspored-nastave-i-ispita
```

## Getting started

Install dependencies:
```
npm install
```

Start the application:
```
npm start
```

You can now access the FERIT Planner web app at `http://localhost:3000`.