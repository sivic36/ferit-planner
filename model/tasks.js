var mongoose = require('mongoose');

var taskSchema = new mongoose.Schema({  
  text: {
      type: String,
      required: true
  },
  completed: {
      type: Boolean,
      required: true
  },
  user: {
      type: String,
      required: true
  }
});
mongoose.model('Task', taskSchema);