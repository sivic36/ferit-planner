var mongoose = require('mongoose');
require('dotenv').config();

console.log("MongoDB connection URL: " + String(process.env.MONGODB_URL))

mongoose.connect(String(process.env.MONGODB_URL))
  .then(console.log)
  .catch(console.error);