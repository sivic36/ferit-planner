var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({  
  name: {
      type: String,
      required: true
  },
  email: {
      type: String,
      required: true
  },
  password: {
      type: String,
      required: true
  },
  studij: {
      naziv:{
          type: String,
          required: true
      },
      smjer:{
          naziv:{
              type: String,
              required: true
          },
          link:{
              type: String,
              required: true
          }
      }
  },
  groups:[{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Event'
  }],
  date: {
      type: Date,
      default: Date.now
  }
});
mongoose.model('User', userSchema);