var mongoose = require('mongoose');

var eventSchema = new mongoose.Schema({  
  text: {
      type: String,
      required: true
  },
  start_date: {
      type: Date,
      required: true
  },
  end_date: {
      type: Date,
      required: true
  },
  color: {
      type: String,
  },
  user: {
      type: String,
      required: true
  }, 
  members: [{
      email: {
          type: String,
          required: true
      },
      accepted: {
          type: Boolean,
          default: false
      }
  }]
});
mongoose.model('Event', eventSchema);